#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
"""
CS224N 2018-19: Homework 5
将每句话进行padding成统一长度，再将每个词padding成统一字符长度，
每句话都会表示成SENT_LEN*WORD_LEN*CHAR_EMBEDDING
对于每个词，表示成WORD_LEN*CHAR_EMBEDDING，对其使用卷积操作，得到eword（输出通道）×(WORD_LEN−k+1)
在进行最大池化，就能够得到和eword一样大小的词向量，与传统的word embedding相比会有更少的参数
"""

### YOUR CODE HERE for part 1i
class CNN(nn.Module):
    def __init__(self,e_char, e_word, max_word_length, kernel_size=5 ):
        super(CNN,self).__init__()
        self.conv=nn.Conv1d(kernel_size=kernel_size,bias=True,in_channels=e_char,out_channels=e_word)
        self.max_pool_1d = nn.MaxPool1d(max_word_length - kernel_size + 1)
    def forward(self,x_reshape):
        x_conv=self.conv(x_reshape)
        x_conv = self.max_pool_1d(F.relu_(x_conv)).squeeze()
        return x_conv
### END YOUR CODE

