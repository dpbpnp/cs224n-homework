#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import torch.nn as nn
import torch
"""
CS224N 2018-19: Homework 5
"""

### YOUR CODE HERE for part 1h
class Highway(nn.Module):
    def __init__(self,embed_word):
        super(Highway,self).__init__()
        self.proj=nn.Linear(embed_word,embed_word)
        self.gate=nn.Linear(embed_word,embed_word)
        self.relu=nn.ReLU()
    def forward(self,conv_out):
        projection=self.relu(self.proj(conv_out))
        gate=torch.sigmoid((self.proj(conv_out)))
        higway=projection*gate+(1-gate)*conv_out
        return higway


### END YOUR CODE 

