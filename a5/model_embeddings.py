#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
model_embeddings.py: Embeddings for the NMT model
Pencheng Yin <pcyin@cs.cmu.edu>
Sahil Chopra <schopra8@stanford.edu>
Anand Dhoot <anandd@stanford.edu>
Michael Hahn <mhahn2@stanford.edu>
"""

import torch.nn as nn
from torch.nn.functional import embedding
from cnn import CNN
from highway import Highway
# Do not change these imports; your module names should be
#   `CNN` in the file `cnn.py`
#   `Highway` in the file `highway.py`
# Uncomment the following two imports once you're ready to run part 1(j)

# from cnn import CNN
# from highway import Highway

# End "do not change" 

class ModelEmbeddings(nn.Module): 
    """
    Class that converts input words to their CNN-based embeddings.
    """
    def __init__(self, embed_size, vocab):
        """
        Init the Embedding layer for one language
        @param embed_size (int): Embedding size (dimensionality) for the output 
        @param vocab (VocabEntry): VocabEntry object. See vocab.py for documentation.
        """
        super(ModelEmbeddings, self).__init__()

        self.embed_size=embed_size
        self.char_embed=50
        self.dropout_rate=0.3
        self.max_word_len= 21
        self.vocab_len=len(vocab.char2id.keys())
        ## A4 code
        pad_token_idx = vocab.char2id['<pad>']
        self.char_embeddings = nn.Embedding(self.vocab_len, self.char_embed, padding_idx=pad_token_idx)
        self.cnn=CNN(self.char_embed,embed_size,self.max_word_len)
        self.highway=Highway(embed_size)
        self.dropout=nn.Dropout(self.dropout_rate)
        ## End A4 code

        ### YOUR CODE HERE for part 1j
        ### END YOUR CODE

    def forward(self, input):
        """
        Looks up character-based CNN embeddings for the words in a batch of sentences.
        @param input: Tensor of integers of shape (sentence_length, batch_size, max_word_length) where
            each integer is an index into the character vocabulary

        @param output: Tensor of shape (sentence_length, batch_size, embed_size), containing the 
            CNN-based embeddings for each word of the sentences in the batch
        """

        ### YOUR CODE HERE for part 1j
        embedding_char=self.char_embeddings(input)#sentence_length, batch_size, max_word_length
        sent_len,batch_size,max_word_length,embedsize=embedding_char.shape[0],embedding_char.shape[1],embedding_char.shape[2],embedding_char.shape[3]
        embedding_char=embedding_char.view(sent_len*batch_size,self.char_embed,max_word_length)
        embedding_vocab=self.cnn(embedding_char)#sent_len*batch_size,max_word_length
        embedding_vocab=self.highway(embedding_vocab)
        embedding_vocab=self.dropout(embedding_vocab)
        embedding_vocab=embedding_vocab.view(sent_len,batch_size,self.embed_size)
        ### END YOUR CODE
        return embedding_vocab

