# part 1

## (a)

随机梯度下降的更新规则：
$$
\begin {align}
\theta \leftarrow \theta -\alpha \bigtriangledown_{\theta} J_{minibatch}(\theta)
\end {align}
$$
$\theta$是模型的参数，$J$是损失函数$\bigtriangledown_{\theta} J_{minibatch}(\theta)$是批梯度，$\alpha $是学习率

$i.$

adam使用动量策略$m$，来累计梯度：
$$
\begin {align}
&m \leftarrow \beta_1m+(1-\beta_1)\bigtriangledown_{\theta} J_{minibatch}(\theta)\\
&\theta \leftarrow \theta -\alpha m
\end {align}
$$
其中第一个等式能够进行梯度累计，当梯度方向与原来的方向一致的时候，动量$m$会增加，否则会进行正负之间的相减，动量$m$会减小，也就是相当于物理的动量，方向相同继续保持。可以抵消噪声，但是当学习率选择不恰当，会出现震荡或者发散的情况。

$ii.$

Adam通过$v$使用自适应的学习率：
$$
\begin {align}
&m \leftarrow \beta_1m+(1-\beta_1)\bigtriangledown_{\theta} J_{minibatch}(\theta)\\
&v\leftarrow \beta_2m+(1-\beta_2m)(\bigtriangledown_{\theta} J_{minibatch}(\theta)\odot\bigtriangledown_{\theta} J_{minibatch}(\theta)\\
&\theta \leftarrow \theta -\alpha \odot m /\sqrt v
\end {align}
$$
$m$仍然是对梯度的平均累计，但是$v$对梯度的元素的平方进行累计，最后将梯度除以了$\sqrt v$进行规范化。使得比较密集的梯度参数更新比较慢，但是稀疏的梯度参数更新比较快，也就是较大的梯度学习率较低，梯度较小的时候学习率会变大。但是通过对梯度的平方进行累计，会使得梯度急剧变小。

## (b)

i.

$\gamma$ = 1/p for scaling output on training (Not sure)

ii.

Dropout 在训练时采用，是为了减少神经元对部分上层神经元的依赖，类似将多个不同网络结构的模型集成起来，减少过拟合的风险。而在测试时，应该用整个训练好的模型，因此不需要dropout。

# part 2

## (a)

注意parsed→I表示的是“I依赖于parsed”，右边是栈顶，所以右边的是最近一个入栈的，一句给的样例，transition每次所做的决定应该如下：

| stack                        | buffer                                 | new dependency   | transition                | step |
| ---------------------------- | -------------------------------------- | ---------------- | ------------------------- | ---- |
| [ROOT]                       | [I, parsed, this, sentence, correctly] |                  | **Initial Configuration** | 0    |
| [ROOT,I]                     | [parsed, this, sentence, correctly]    |                  | SHIFT                     | 1    |
| [ROOT,I,parsed]              | [this, sentence, correctly]            |                  | SHIFT                     | 2    |
| [ROOT, parsed]               | [this, sentence, correctly]            | parsed→I         | LEFT-ARC                  | 3    |
| [ROOT, parsed,this]          | [sentence, correctly]                  |                  | SHIFT                     | 4    |
| [ROOT, parsed,this,sentence] | [correctly]                            |                  | SHIFT                     | 5    |
| [ROOT, parsed,sentence]      | [correctly]                            | sentence→this    | LEFT-ARC                  | 6    |
| [ROOT, parsed]               | [correctly]                            | parsed→sentence  | RIGHT-ARC                 | 7    |
| [ROOT, parsed,correctly]     | []                                     |                  | SHIFT                     | 8    |
| [ROOT, parsed]               | []                                     | parsed→correctly | RIGHT-ARC                 | 9    |
| [ROOT]                       | []                                     | ROOT→parsed      | RIGHT-ARC                 | 10   |

## (b)

需要2N次。因为"SHIFT"操作需要读取N个单词，每个单词需要"ARC"操作建立依赖关系，从栈中移除，需要N次，当完成parsing的时候栈内只剩下"ROOT"。因此总共需要N(SHIFT)+N(ARC)=2N次操作。

