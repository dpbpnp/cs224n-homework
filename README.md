# cs224n课后作业

#### 介绍
cs224n 2019年课后作业，有部分回答参考了https://codechina.csdn.net/mirrors/Luvata/CS224N-2019?utm_source=csdn_github_accelerator
课程网址：https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1194/

#### 软件架构
软件架构说明
Python 3.7.9
所安装的包说明
absl-py                   0.11.0                   pypi_0    pypi
aiohttp                   3.7.4            py37h2bbff1b_1    defaults
argon2-cffi               20.1.0           py37h2bbff1b_1    defaults
astor                     0.8.1                    pypi_0    pypi
astunparse                1.6.3                    pypi_0    pypi
async-timeout             3.0.1            py37haa95532_0    defaults
async_generator           1.10             py37h28b3542_0    defaults
attrs                     20.3.0             pyhd3eb1b0_0    defaults
autopep8                  1.5.6                    pypi_0    pypi
backcall                  0.2.0              pyhd3eb1b0_0    defaults
blas                      1.0                         mkl    defaults
bleach                    3.3.0              pyhd3eb1b0_0    defaults
boto3                     1.17.9                   pypi_0    pypi
botocore                  1.20.9                   pypi_0    pypi
bottle                    0.12.19                  pypi_0    pypi
brotlipy                  0.7.0           py37h2bbff1b_1003    defaults
ca-certificates           2021.5.30            h5b45459_0    https://mirrors.ustc.edu.cn/anaconda/cloud/conda-forge
cachetools                4.2.0                    pypi_0    pypi
certifi                   2021.5.30        py37h03978a9_0    https://mirrors.ustc.edu.cn/anaconda/cloud/conda-forge
cffi                      1.14.5           py37hcd4344a_0    defaults
chardet                   4.0.0                    pypi_0    pypi
click                     8.0.1              pyhd3eb1b0_0    defaults
colorama                  0.4.4              pyhd3eb1b0_0    defaults
common                    0.1.2                    pypi_0    pypi
coverage                  5.5              py37h2bbff1b_2    defaults
cryptography              3.4.7            py37h71e12ea_0    defaults
cudatoolkit               10.2.89              h74a9793_1    defaults
cycler                    0.10.0                   py37_0    defaults
cython                    0.29.14                  pypi_0    pypi
data                      0.4                      pypi_0    pypi
decorator                 4.4.2              pyhd3eb1b0_0    defaults
defusedxml                0.7.1              pyhd3eb1b0_0    defaults
display                   1.0.0                    pypi_0    pypi
dual                      0.0.10                   pypi_0    pypi
dynamo3                   0.4.10                   pypi_0    pypi
entrypoints               0.3                      py37_0    defaults
et-xmlfile                1.1.0                    pypi_0    pypi
flatbuffers               1.12                     pypi_0    pypi
flywheel                  0.5.4                    pypi_0    pypi
freetype                  2.10.4               hd328e21_0    defaults
funcsigs                  1.0.2                    pypi_0    pypi
gast                      0.3.3                    pypi_0    pypi
gensim                    3.0.0                    pypi_0    pypi
google-api-core           1.25.1             pyhd3eb1b0_0    defaults
google-auth               1.24.0                   pypi_0    pypi
google-auth-oauthlib      0.4.2                    pypi_0    pypi
google-cloud-core         1.7.1              pyhd3eb1b0_0    defaults
google-cloud-storage      1.40.0             pyhd3eb1b0_0    defaults
google-crc32c             1.1.2            py37h2bbff1b_0    defaults
google-pasta              0.2.0                    pypi_0    pypi
google-resumable-media    1.3.1              pyhd3eb1b0_1    defaults
googleapis-common-protos  1.53.0           py37h2eaa2aa_0    defaults
grpcio                    1.32.0                   pypi_0    pypi
h5py                      2.10.0                   pypi_0    pypi
icc_rt                    2019.0.0             h0cc432a_1    anaconda
icu                       58.2                 ha925a31_3    defaults
idna                      2.10               pyhd3eb1b0_0    defaults
importlib-metadata        3.4.0                    pypi_0    pypi
importlib_metadata        2.0.0                         1    defaults
intel-openmp              2020.2                      254    defaults
ipykernel                 5.5.0                    pypi_0    pypi
ipython                   7.21.0           py37hd4e2768_0    defaults
ipython_genutils          0.2.0              pyhd3eb1b0_1    defaults
ipywidgets                7.6.3              pyhd3deb0d_0    conda-forge
jedi                      0.18.0                   pypi_0    pypi
jinja2                    2.11.3             pyhd3eb1b0_0    defaults
jmespath                  0.10.0                     py_0    defaults
joblib                    0.17.0                     py_0    anaconda
jpeg                      9b                   hb83a4c4_2    defaults
jsonschema                3.2.0                      py_2    defaults
jupyter-client            6.1.12                   pypi_0    pypi
jupyter_client            6.1.7                      py_0    defaults
jupyter_contrib_core      0.3.3                      py_2    conda-forge
jupyter_contrib_nbextensions 0.5.1              pyhd8ed1ab_2    conda-forge
jupyter_core              4.7.1            py37haa95532_0    defaults
jupyter_highlight_selected_word 0.2.0           py37h03978a9_1002    conda-forge
jupyter_latex_envs        1.4.6           pyhd8ed1ab_1002    conda-forge
jupyter_nbextensions_configurator 0.4.1            py37h03978a9_2    conda-forge
jupyterlab_pygments       0.1.2                      py_0    defaults
jupyterlab_widgets        1.0.0              pyhd3eb1b0_1    defaults
keras-preprocessing       1.1.2                    pypi_0    pypi
kiwisolver                1.3.0            py37hd77b12b_0    defaults
libcrc32c                 1.1.1                ha925a31_2    defaults
libiconv                  1.16                 he774522_0    conda-forge
libpng                    1.6.37               h2a8f88b_0    defaults
libprotobuf               3.14.0               h23ce68f_0    defaults
libsodium                 1.0.18               h62dcd97_0    defaults
libtiff                   4.1.0                h56a325e_1    defaults
libuv                     1.40.0               he774522_0    defaults
libxml2                   2.9.10               hf5bbc77_3    conda-forge
libxslt                   1.1.33               h65864e5_2    conda-forge
lxml                      4.6.2            py37hd07aab1_1    conda-forge
lz4-c                     1.9.3                h2bbff1b_0    defaults
m2w64-gcc-libgfortran     5.3.0                         6    defaults
m2w64-gcc-libs            5.3.0                         7    defaults
m2w64-gcc-libs-core       5.3.0                         7    defaults
m2w64-gmp                 6.1.0                         2    defaults
m2w64-libwinpthread-git   5.0.0.4634.697f757               2    defaults
markdown                  3.3.3                    pypi_0    pypi
markupsafe                1.1.1            py37hfa6e2cd_1    defaults
matplotlib                3.3.2                haa95532_0    defaults
matplotlib-base           3.3.2            py37hba9282a_0    defaults
mistune                   0.8.4           py37hfa6e2cd_1001    defaults
mkl                       2020.2                      256    defaults
mkl-service               2.3.0            py37h196d8e1_0    defaults
mkl_fft                   1.2.0            py37h45dec08_0    defaults
mkl_random                1.1.1            py37h47e9c7a_0    defaults
msys2-conda-epoch         20160418                      1    defaults
multidict                 5.1.0            py37h2bbff1b_2    defaults
nb_conda                  2.2.1            py37h03978a9_4    https://mirrors.ustc.edu.cn/anaconda/cloud/conda-forge
nb_conda_kernels          2.3.1            py37h03978a9_0    https://mirrors.ustc.edu.cn/anaconda/cloud/conda-forge
nbclient                  0.5.3              pyhd3eb1b0_0    defaults
nbconvert                 6.0.7                    py37_0    defaults
nbformat                  5.1.2              pyhd3eb1b0_1    defaults
nest-asyncio              1.5.1              pyhd3eb1b0_0    defaults
networkx                  2.5                        py_0    defaults
ninja                     1.10.2           py37h6d14046_0    defaults
nltk                      3.6.2              pyhd3eb1b0_0    defaults
notebook                  6.2.0            py37haa95532_0    defaults
numpy                     1.19.3                   pypi_0    pypi
numpy-base                1.19.2           py37ha3acd2a_0    defaults
oauthlib                  3.1.0                    pypi_0    pypi
olefile                   0.46                     py37_0    defaults
opencv-python             4.5.2.52                 pypi_0    pypi
openpyxl                  3.0.7                    pypi_0    pypi
openssl                   1.1.1k               h8ffe710_0    https://mirrors.ustc.edu.cn/anaconda/cloud/conda-forge
opt-einsum                3.3.0                    pypi_0    pypi
packaging                 20.9               pyhd3eb1b0_0    defaults
paddle                    1.0.2                    pypi_0    pypi
paddlepaddle              2.0.0                    pypi_0    pypi
pandas                    1.2.1            py37hf11a4ad_0    defaults
pandoc                    2.11                 h9490d1a_0    defaults
pandocfilters             1.4.3            py37haa95532_1    defaults
parso                     0.8.1              pyhd3eb1b0_0    defaults
peewee                    3.14.1                   pypi_0    pypi
pgl                       2.1                      pypi_0    pypi
pickleshare               0.7.5           pyhd3eb1b0_1003    defaults
pillow                    8.1.0            py37h4fa10fc_0    defaults
pip                       20.3.3           py37haa95532_0    https://repo.anaconda.com/pkgs/main
prometheus_client         0.9.0              pyhd3eb1b0_0    defaults
prompt-toolkit            3.0.17                   pypi_0    pypi
protobuf                  3.14.0                   pypi_0    pypi
prox                      0.0.17                   pypi_0    pypi
pyasn1                    0.4.8                      py_0    defaults
pyasn1-modules            0.2.8                    pypi_0    pypi
pycodestyle               2.7.0                    pypi_0    pypi
pycparser                 2.20                       py_2    defaults
pygments                  2.8.1              pyhd3eb1b0_0    defaults
pyopenssl                 20.0.1             pyhd3eb1b0_1    defaults
pyparsing                 2.4.7              pyhd3eb1b0_0    defaults
pyqt                      5.9.2            py37h6538335_2    defaults
pyrsistent                0.17.3           py37he774522_0    defaults
pysocks                   1.7.1                    py37_1    defaults
python                    3.7.9                h60c2a47_0    https://repo.anaconda.com/pkgs/main
python-dateutil           2.8.1                      py_0    defaults
python-geoip-python3      1.3                      pypi_0    pypi
python-levenshtein        0.12.2           py37h2bbff1b_0    defaults
python_abi                3.7                     1_cp37m    conda-forge
pytorch                   1.7.1           py3.7_cuda102_cudnn7_0    pytorch
pytz                      2020.5             pyhd3eb1b0_0    defaults
pywin32                   300                      pypi_0    pypi
pywinpty                  0.5.7                    py37_0    defaults
pyyaml                    5.4.1                    pypi_0    pypi
pyzmq                     22.0.3                   pypi_0    pypi
qt                        5.9.7            vc14h73c81de_0    defaults
regex                     2021.7.6         py37h2bbff1b_0    defaults
requests                  2.25.1             pyhd3eb1b0_0    defaults
requests-oauthlib         1.3.0                    pypi_0    pypi
rsa                       4.7                      pypi_0    pypi
s3transfer                0.3.4                    pypi_0    pypi
scikit-learn              0.24.1           py37hf11a4ad_0    defaults
scipy                     1.6.1            py37h14eb087_0    defaults
send2trash                1.5.0              pyhd3eb1b0_1    defaults
setuptools                51.3.3           py37haa95532_4    https://repo.anaconda.com/pkgs/main
sip                       4.19.8           py37h6538335_0    defaults
six                       1.15.0           py37haa95532_0    defaults
smart_open                5.1.0              pyhd3eb1b0_0    defaults
sqlite                    3.33.0               h2a8f88b_0    https://repo.anaconda.com/pkgs/main
tensorboard               2.4.1                    pypi_0    pypi
tensorboard-plugin-wit    1.8.0                    pypi_0    pypi
tensorflow                2.4.1                    pypi_0    pypi
tensorflow-estimator      2.4.0                    pypi_0    pypi
termcolor                 1.1.0                    pypi_0    pypi
terminado                 0.9.2            py37haa95532_0    defaults
testpath                  0.4.4              pyhd3eb1b0_0    defaults
threadpoolctl             2.1.0              pyh5ca1d4c_0    anaconda
tight                     0.1.0                    pypi_0    pypi
tk                        8.6.10               he774522_0    defaults
toml                      0.10.2                   pypi_0    pypi
torchaudio                0.7.2                      py37    pytorch
torchvision               0.8.2                py37_cu102    pytorch
tornado                   6.1              py37h2bbff1b_0    defaults
tqdm                      4.56.0             pyhd3eb1b0_0    defaults
traitlets                 5.0.5              pyhd3eb1b0_0    defaults
typing-extensions         3.7.4.3                       0    defaults
typing_extensions         3.7.4.3                    py_0    defaults
urllib3                   1.26.2                   pypi_0    pypi
vc                        14.2                 h21ff451_1    https://repo.anaconda.com/pkgs/main
vs2015_runtime            14.27.29016          h5e58377_2    https://repo.anaconda.com/pkgs/main
wcwidth                   0.2.5                      py_0    defaults
webencodings              0.5.1                    py37_1    defaults
werkzeug                  1.0.1                    pypi_0    pypi
wheel                     0.36.2             pyhd3eb1b0_0    https://repo.anaconda.com/pkgs/main
widgetsnbextension        3.5.1                    py37_0    defaults
win_inet_pton             1.1.0            py37haa95532_0    defaults
wincertstore              0.2                      py37_0    https://repo.anaconda.com/pkgs/main
winpty                    0.4.3                         4    defaults
wrapt                     1.12.1                   pypi_0    pypi
xlrd                      2.0.1                    pypi_0    pypi
xz                        5.2.5                h62dcd97_0    defaults
yaml                      0.2.5                he774522_0    conda-forge
yarl                      1.5.1            py37he774522_0    defaults
zeromq                    4.3.3                ha925a31_3    defaults
zipp                      3.4.0              pyhd3eb1b0_0    defaults
zlib                      1.2.11               h62dcd97_4    https://repo.anaconda.com/pkgs/main
zstd                      1.4.5                h04227a9_0    defaults


#### 使用说明

1.  转载麻烦注明出处

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
