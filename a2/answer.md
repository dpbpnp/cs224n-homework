# answer

损失函数为
$$
\widehat{y}_o=softmax(v_ov_c)=\frac{e^{v_ov_c}}{-\sum_{w=1}^{V}e^{v_wv_c}}  \\
J(\theta)=-\sum_{w=1}^{V}y_wlog(\widehat{y}_w)
$$


## （a）

对于
$$
y_w\begin{cases}1,w=o\\0,w\neq o \end{cases} \\
-\sum_{w=1}^{V}y_wlog(\widehat{y}_w)=-log(\widehat{y}_w)
$$

$$
其中V表示词汇表的大小，\widehat{y}_w表示预测值
$$

## （b）

$$
J_{naive-softmax}(v_c,U,o)=-log(\widehat{y}_o)=-log(\frac{e^{v_ov_c}}{-\sum_{w=1}^{V}e^{v_wv_c}}) \\
$$

**损失函数对于中心词的偏导数**
$$
\frac{\partial J_{naive-softmax}(v_c,U,o)}{\partial v_c}=-(\frac{\partial log(\frac{e^{v_ov_c}}{-\sum_{w=1}^{V}e^{v_wv_c}})}{\partial v_c})\\
=-(\frac{\partial (log(e^{v_ov_c})-log(\sum_{w=1}^{V}e^{v_wv_c}))}{\partial v_c})\\
=-(\frac{\partial log(e^{v_ov_c})}{\partial v_c}-\frac{\partial log(\sum_{w=1}^{V}e^{v_wv_c})}{\partial v_c})\\
=-(\frac{\partial v_ov_c}{\partial v_c}-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\frac{\partial \sum_{w=1}^{V}e^{v_wv_c}}{\partial v_c})\\
=-(v_o-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\sum_{w=1}^{V} \frac{\partial e^{v_wv_c}}{\partial v_c})\\
=-(v_o-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\sum_{w=1}^{V}e^{v_wv_c}v_w)\\
=-(v_o-\sum_{w=1}^{V}\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}e^{v_wv_c}v_w)\\
=-(v_o-\sum_{w=1}^{V}\frac{e^{v_wv_c}}{\sum_{w=1}^{V}e^{v_wv_c}}v_w)\\
=-（v_o-\sum_{w=1}^{V}\widehat {y}_w v_w)\\
=\sum_{w=1}^{V}\widehat {y}_w v_w-(v_o*1-\sum_{w=1}^{V}\widehat v_w*0)\\
=\sum_{w=1}^{V}\widehat {y}_w v_w-\sum_{w=1}^{V}y_w v_w
=U^T-(\widehat {Y}-Y)
$$

其中$U$为上下文词嵌入，$\widehat {Y}_w$为预测值，$Y$为真实值

## (c)

**损失函数对于上下文的偏导数**

case1:当前单词o属于中心词的上下文
$$
\begin{align}
&\frac{\partial J_{naive-softmax}(v_c,U,o)}{\partial v_o}=-(\frac{\partial log(\frac{e^{v_ov_c}}{-\sum_{w=1}^{V}e^{v_wv_c}})}{\partial v_o})\\
&=-(\frac{\partial (log(e^{v_ov_c})-log(\sum_{w=1}^{V}e^{v_wv_c}))}{\partial v_o})\\
&=-(\frac{\partial log(e^{v_ov_c})}{\partial v_o}-\frac{\partial log(\sum_{w=1}^{V}e^{v_wv_c})}{\partial v_o})\\
&=-(\frac{\partial v_ov_c}{\partial v_o}-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\frac{\partial \sum_{w=1}^{V}e^{v_wv_c}}{\partial v_o})\\
&=-(v_c-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\sum_{w=1}^{V} \frac{\partial e^{v_wv_c}}{\partial v_o})\\
&=-(v_c-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}} \frac{\partial e^{v_wv_c}}{\partial v_o})\\
&=-(v_c-\frac{e^{v_ov_c}}{\sum_{w=1}^{V}e^{v_wv_c}} \frac{ \partial v_ov_c}{\partial v_o})\\
&=-(v_c-\frac{e^{v_ov_c}}{\sum_{w=1}^{V}e^{v_wv_c}} v_c)\\
&=-(v_c-\widehat y_o v_c)\\
&=v_c(\widehat y_o-1)\\
&=v_c(\widehat y_o-y_n)\\
\end{align}
$$
case2:当前单词w属于不中心词的上下文

$$
\begin{align}
&\frac{\partial J_{naive-softmax}(v_c,U,o)}{\partial v_n}=-(\frac{\partial log(\frac{e^{v_ov_c}}{-\sum_{w=1}^{V}e^{v_wv_c}})}{\partial v_n})\\
&=-(\frac{\partial (log(e^{v_ov_c})-log(\sum_{w=1}^{V}e^{v_wv_c}))}{\partial v_n})\\
&=-(\frac{\partial log(e^{v_ov_c})}{\partial v_n}-\frac{\partial log(\sum_{w=1}^{V}e^{v_wv_c})}{\partial v_n})\\
&=-(\frac{\partial v_ov_c}{\partial v_n}-\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\frac{\partial \sum_{w=1}^{V}e^{v_wv_c}}{\partial v_n})\\
&=-(\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}}\sum_{w=1}^{V} \frac{\partial e^{v_wv_c}}{\partial v_n})\\
&=-(\frac{1}{\sum_{w=1}^{V}e^{v_wv_c}} \frac{\partial e^{v_nv_c}}{\partial v_n})\\
&=-(\frac{e^{v_nv_c}}{\sum_{w=1}^{V}e^{v_wv_c}} \frac{ \partial v_nv_c}{\partial v_n})\\
&=-(\frac{e^{v_ov_c}}{\sum_{w=1}^{V}e^{v_wv_c}} v_c)\\
&=-(v_c*0-\widehat y_o v_c)\\
&=v_c(\widehat y_n-y_n)\\
\end{align}
$$
综上所述，无论是对于是否是真实上下文还是非上下文词汇，其词向量的梯度如下方式计算：
$$
v_c(\widehat y_n-y_n)
$$

## (d)

$$
\begin{align}
&\sigma(x)=\frac{1}{1+e^{-x}}=\frac{e^x}{1+e^x}\\
&\frac{\partial \sigma(x)}{\partial x}=\frac {\partial\frac { e^x} {1+e^x}}{\partial x}\\
&=\frac{e^x(1+e^x)-e^xe^x}{(1+e^x)^2}\\
&=\frac{e^x}{(1+e^x)^2}\\
&=\frac{1+e^x-1}{(1+e^x)^2}\\
&=\frac{1}{1+e^x}-\frac{1}{(1+e^x)^2}\\
&=\frac{1}{1+e^x}(1-\frac{1}{1+e^x})\\
&=\sigma(x)(1-\sigma(x))
\end{align}
$$

## (e)

**对于中心词而言：**
$$
\begin{align}
&\frac{\partial J_{neg-sample}(v_c,o,U)}{\partial v_c}=-\frac {\partial log(\sigma (u_ov_c)) +\sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_c}\\
&=-[\frac {\partial log(\sigma (u_ov_c))}{\partial v_c}+\frac {\partial \sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_c}]\\
&=-[\frac {1}{\sigma (u_ov_c)}\frac {\partial \sigma(u_ov_c)}{\partial v_c}+\sum_{k=1}^K \frac {\partial  log(\sigma (-u_kv_c)}{\partial v_c}]\\
&=-[\frac {1}{\sigma (u_ov_c)}{\sigma(u_ov_c)(1-\sigma(u_ov_c))v_o}+\sum_{k=1}^K \frac {1}{\sigma (-u_kv_c)}\frac {\partial \sigma(-u_kv_c)}{\partial v_c}]\\
&=-[(1-\sigma(u_ov_c))v_o-\sum_{k=1}^K \frac {1}{\sigma (-u_kv_c)}{\sigma(-u_kv_c)(1-\sigma(-u_kv_c))v_k}]\\
&=-[(1-\sigma(u_ov_c))v_o-\sum_{k=1}^K (1-\sigma(-u_kv_c))v_k]\\
\end{align}
$$
**对于当前词不是正样本：**
$$
\begin{align}
&\frac{\partial J_{neg-sample}(v_c,o,U)}{\partial v_k}=-\frac {\partial log(\sigma (u_ov_c)) +\sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_k}\\
&=-[\frac {\partial log(\sigma (u_ov_c))}{\partial v_k}+\frac {\partial \sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_k}]\\
&=- \frac {\partial  log(\sigma (-u_kv_c)}{\partial v_k}\\
&=-\frac {1}{\sigma (-u_kv_c)}\frac {\partial \sigma(-u_kv_c)}{\partial v_k}\\
&=\frac {1}{\sigma (-u_kv_c)}{\sigma(-u_kv_c)(1-\sigma(-u_kv_c))v_c}\\
&=(1-\sigma(-u_kv_c))v_c
\end{align}
$$
**对于当前词是正样本：**
$$
\begin{align}
&\frac{\partial J_{neg-sample}(v_c,o,U)}{\partial v_o}=-\frac {\partial log(\sigma (u_ov_c)) +\sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_o}\\
&=-[\frac {\partial log(\sigma (u_ov_c))}{\partial v_o}+\frac {\partial \sum_{k=1}^K log(\sigma (-u_kv_c)}{\partial v_o}]\\
&=-\frac {1}{\sigma (u_ov_c)}\frac {\partial \sigma(u_ov_c)}{\partial v_o}\\
&=-\frac {1}{\sigma (u_ov_c)}{\sigma(u_ov_c)（1-\sigma(u_ov_c))v_c}\\
&=-(1-\sigma(u_ov_c))v_c\\
\end{align}
$$

## （f）

$$
\begin{align}&\frac{\partial J_{skip-gram}(v_c,w_{t-m},...,w_{t+m},U)}{\partial U}=-\sum_{-m<=j<=m,j!=0} \frac {\partial J_{skip-gram}(v_c,w_{t+j},U)}{\partial  U}
\end{align}
$$



当w=c
$$
\begin{align}&\frac{\partial J_{skip-gram}(v_c,w_{t-m},...,w_{t+m},U)}{\partial v_c}=-\sum_{-m<=j<=m,j!=0} \frac {\partial J_{skip-gram}(v_c,w_{t+j},U)}{\partial  v_c}
\end{align}
$$
当w!=c
$$
\begin{align}&\frac{\partial J_{skip-gram}(v_c,w_{t-m},...,w_{t+m},U)}{\partial v_w}=0
\end{align}
$$


